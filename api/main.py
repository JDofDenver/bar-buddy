from fastapi import FastAPI
from authenticator import authenticator
from routers import accounts, cocktails, favorites, ratings
from fastapi.middleware.cors import CORSMiddleware
import os

app = FastAPI()

origins = [
    "http://localhost:8000",
    "http://localhost:3000",
    os.environ.get("CORS_HOST", None),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(authenticator.router, tags=["Login"])
app.include_router(accounts.router, tags=["Sign_Up"])
app.include_router(cocktails.router, tags=["Cocktails"])
app.include_router(favorites.router, tags=["Favorites"])
app.include_router(ratings.router, tags=["Ratings"])
