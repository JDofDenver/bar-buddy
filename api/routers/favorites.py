from fastapi import APIRouter, Depends, HTTPException
from models.favorites import FavoriteIn, FavoritesList, DeleteStatus
from authenticator import authenticator
from queries.favorites import FavoritesQueries


router = APIRouter()


@router.post("/api/favorites/", response_model=FavoritesList)
def create_favorite(
    favorite_in: FavoriteIn,
    favorite_queries: FavoritesQueries = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    favorite = favorite_queries.create_favorite(
        account_id=account["id"], favorite_in=favorite_in
    )
    if favorite is None:
        raise HTTPException(status_code=500,
                            detail="Failed to create favorite")

    return FavoritesList(favorites=[favorite])


@router.get("/api/favorites", response_model=FavoritesList)
def list_favorites(
    account: dict = Depends(authenticator.get_current_account_data),
    favorite_queries: FavoritesQueries = Depends(),
):
    return {"favorites": favorite_queries.get_all_favorites(account["id"])}


@router.delete("/api/favorites/{favorite_id}", response_model=DeleteStatus)
def delete_favorite(
    favorite_id: str,
    account: dict = Depends(authenticator.get_current_account_data),
    queries: FavoritesQueries = Depends(),
):
    return {"success": queries.delete(favorite_id, account["id"])}
