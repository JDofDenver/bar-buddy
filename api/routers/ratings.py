from fastapi import APIRouter, Depends
from typing import Union
from authenticator import authenticator
from queries.ratings import RatingsQueries
from models.ratings import (RatingAverage,
                            RatingIn,
                            RatingOut,
                            RatingsList,
                            RatingsCount)

router = APIRouter()


@router.get("/api/ratings/{cocktail_id}", response_model=RatingsList)
def get_cocktail_ratings(
    cocktail_id: str,
    ratings_queries: RatingsQueries = Depends(),
):
    return {
        "ratings": ratings_queries.get_cocktail_ratings(
            cocktail_id=cocktail_id
        )
    }


@router.get("/api/ratings/{cocktail_id}/avg", response_model=RatingAverage)
def get_avg_cocktail_ratings(
    cocktail_id: str,
    ratings_queries: RatingsQueries = Depends(),
):
    return {
        "ratings_average": ratings_queries.get_avg_cocktail_ratings(
            cocktail_id=cocktail_id
        )
    }


@router.get("/api/ratings/{cocktail_id}/count", response_model=RatingsCount)
def get_cocktail_ratings_count(
    cocktail_id: str, ratings_queries: RatingsQueries = Depends()
):
    return {
        "ratings_count": ratings_queries.get_cocktail_ratings_count(
            cocktail_id=cocktail_id
        )
    }


@router.get("/api/ratings/{cocktail_id}/user",
            response_model=Union[RatingOut, None])
def get_user_rating(
    cocktail_id: str,
    ratings_queries: RatingsQueries = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    return ratings_queries.get_user_rating(
        cocktail_id=cocktail_id, account_id=account["id"]
    )


@router.post("/api/ratings/{cocktail_id}/user",
             response_model=Union[RatingOut, None])
def update_or_create_user_rating(
    cocktail_id: str,
    rating_in: RatingIn,
    ratings_queries: RatingsQueries = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    print(rating_in)
    return ratings_queries.update_or_create_user_rating(
        cocktail_id=cocktail_id,
        account_name=account["name"],
        account_id=account["id"],
        rating_in=rating_in,
    )
