from fastapi import APIRouter, Depends, HTTPException
from models.cocktails import CocktailIn, CocktailList, CocktailOut
from queries.cocktails import CocktailQueries
from authenticator import authenticator


router = APIRouter()


@router.get("/api/cocktails/", response_model=CocktailList)
def get_all_cocktails(
    cocktail_queries: CocktailQueries = Depends(),
):
    return {"cocktails": cocktail_queries.get_all_cocktails()}


@router.post("/api/cocktails/", response_model=CocktailList)
def create_cocktail(
    cocktail_in: CocktailIn,
    cocktail_queries: CocktailQueries = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    if account:
        cocktail = cocktail_queries.create_cocktail(
            account_id=account["id"], cocktail_in=cocktail_in
        )
        if cocktail is None:
            raise HTTPException(status_code=500,
                                detail="Failed to create cocktail")

        return CocktailList(cocktails=[cocktail])
    else:
        raise HTTPException(status_code=401, detail="Authentication required")


@router.get("/api/cocktails/{cocktail_id}", response_model=CocktailOut)
def get_one_cocktail(
    cocktail_id: str,
    cocktail_queries: CocktailQueries = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):

    if account:
        cocktail = cocktail_queries.get_one_cocktail(cocktail_id=cocktail_id)
        if not cocktail:
            raise HTTPException(status_code=404, detail="Cocktail not found")
        return cocktail
    else:
        raise HTTPException(status_code=401, detail="Authentication required")


@router.put("/api/cocktails/{cocktail_id}", response_model=CocktailOut)
def update_cocktail(
    cocktail_id: str,
    cocktail_in: CocktailIn,
    cocktail_queries: CocktailQueries = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    if account:
        cocktail = cocktail_queries.get_one_cocktail(cocktail_id)
        if not cocktail:
            raise HTTPException(status_code=404, detail="Cocktail not found")
        if cocktail.account_id != account["id"]:
            raise HTTPException(
                status_code=403, detail="Unauthorized to update the cocktail"
            )

        updated_cocktail = cocktail_queries.update(
            cocktail_id, account["id"], cocktail_in
        )
        if updated_cocktail is None:
            raise HTTPException(status_code=404, detail="Cocktail not found")

        return updated_cocktail
    else:
        raise HTTPException(status_code=401, detail="Authentication required")


@router.delete("/api/cocktails/{cocktail_id}", response_model=bool)
def delete_cocktail(
    cocktail_id: str,
    cocktail_queries: CocktailQueries = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> bool:
    if account:
        cocktail = cocktail_queries.get_one_cocktail(cocktail_id)
        if not cocktail:
            raise HTTPException(status_code=404, detail="Cocktail not found")
        if cocktail.account_id != account["id"]:
            raise HTTPException(
                status_code=403, detail="Unauthorized to update the cocktail"
            )
        deleted_cocktail = cocktail_queries.delete(cocktail_id, account["id"])
        if not deleted_cocktail:
            raise HTTPException(status_code=404, detail="Cocktail not found")

        return True
    else:
        raise HTTPException(status_code=401, detail="Authentication required")
