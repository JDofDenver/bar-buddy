from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token


class AccountOut(BaseModel):
    id: str
    email: str
    name: str
    birthday: str
    city: str
    image: str
    experience: str


class AccountIn(BaseModel):
    email: str
    password: str
    name: str
    birthday: str
    city: str
    image: str
    experience: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut
