from fastapi.testclient import TestClient
from main import app
from queries.cocktails import CocktailQueries

client = TestClient(app)


class FakeCocktailQueries:
    def get_all_cocktails(self):
        return [
            {
                "cocktail_name": "Drink Uno",
                "alcoholic": True,
                "ingredients": "Whiskey",
                "image_url": "https://picsum.photos/200",
                "instructions": "string",
                "suggested_serving_glass": "string",
                "category": "ordinary drink",
                "id": str(i),
                "account_id": "647389493a8f2528631b87e9",
            }
            for i in range(10)
        ]


def test_get_all_cocktails():
    # Arrange
    app.dependency_overrides[CocktailQueries] = FakeCocktailQueries

    # Act
    response = client.get("/api/cocktails/")
    data = response.json()

    # Assert
    assert response.status_code == 200
    assert data == {
        "cocktails": [
            {
                "cocktail_name": "Drink Uno",
                "alcoholic": True,
                "ingredients": "Whiskey",
                "image_url": "https://picsum.photos/200",
                "instructions": "string",
                "suggested_serving_glass": "string",
                "category": "ordinary drink",
                "id": str(i),
                "account_id": "647389493a8f2528631b87e9",
            }
            for i in range(10)
        ]
    }

    # Cleanup
    app.dependency_overrides = {}
