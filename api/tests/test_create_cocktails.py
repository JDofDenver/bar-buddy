from fastapi.testclient import TestClient
from main import app
from models.cocktails import CocktailIn, CocktailOut
from queries.cocktails import CocktailQueries
from authenticator import authenticator
import json

client = TestClient(app)


class FakeCocktailQueries:
    def create_cocktail(
        self,
        account_id: str,
        cocktail_in: CocktailIn,
    ):
        cocktail_dict = cocktail_in.dict()
        cocktail_dict["account_id"] = str(account_id)
        cocktail_dict["id"] = "1234"
        return CocktailOut(**cocktail_dict)


def fake_account_data():
    return {"id": "2030"}


def test_create_cocktail():
    # // Arrange
    app.dependency_overrides[CocktailQueries] = FakeCocktailQueries
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_account_data
    # Act
    cocktail_in = {
        "cocktail_name": "Old Beary",
        "alcoholic": True,
        "ingredients": "Whiskey, Milk, Honey",
        "image_url": "string",
        "instructions": "Savor",
        "suggested_serving_glass": "Tumbler",
        "category": "cocktail",
    }
    res = client.post("/api/cocktails/", json.dumps(cocktail_in))
    data = res.json()
    print(data)

    # // Assert
    assert res.status_code == 200
    assert data["cocktails"][0]["id"] == "1234"

    # cleanup
    app.dependency_overrides = {}
