# BAR BUDDY
![Cocktails Wireframe](images/Drinks.jpg)

Team Members

* Michael Marquart
* Jasmine Bagha
* Jordan Aguirre
* Trav Diehl
* John Dawson

## Design

Bar Buddy is a web application that is designed to help users effortlessly manage their cocktail drinks. It uses FastAPI for smooth operations like creating, reading, updating, and deleting cocktail recipes. The frontend is built with React and Redux, making it easy to navigate and interact with your drink collection. Our application simplifies the process of managing cocktail recipes, offering a user-friendly platform for cocktail enthusiasts. Say goodbye to complicated systems and say hello to a hassle-free way of organizing your favorite drinks.

## Wireframe
![Cocktails Wireframe](images/Wireframe.png)

## Instructions
How to set up the project on your local computer
1. In your terminal create the volume
```
docker volume create drinks-data
```
2. After volumes have been created, build your containers
```
docker compose build
```
3. After containers have been build, run the containers
```
docker compose up
```
4. You can access the web application in you web browser at
```
localhost:3000
```
5. You can access backend crud operations at
```
localhost:8000/docs#
```

![Front End](images/Colorful.jpg)

### Front End
To fully interact with this application, please follow the steps below:
1. [Sign Up](http://localhost:3000/signup)
2. [Log In](http://localhost:3000/login)
3. [Create a drink](http://localhost:3000/cocktails/new)
4. [Favorite a drink](http://localhost:3000/cocktails)
5. Explore the rest of the functionalities

### React
<table>
	<thead>
		<tr>
			<th>Action</th>
			<th>URL</th>
			<th>Component/File</th>
		</tr>
	</thead>
	<tbody>
    <tr>
			<td>Sign Up</td>
			<td>http://localhost:3000/signup</td>
			<td>SignUp</td>
		</tr>
  	<tr>
			<td>Log In</td>
			<td>http://localhost:3000/login</td>
			<td>Login</td>
		</tr>
		<tr>
			<td>Create a cocktail</td>
			<td>http://localhost:3000/cocktails/new</td>
			<td>CocktailForm</td>
		</tr>
		<tr>
			<td>Update a cocktail</td>
			<td>http://localhost:3000/cocktails/update/{cocktail_id}</td>
			<td>UpdateCocktailForm</td>
		</tr>
		<tr>
			<td>Delete a cocktail</td>
			<td>http://localhost:3000/cocktails/delete/{cocktail_id}</td>
			<td>CocktailDeleteButton</td>
		</tr>
		<tr>
			<td>See list of cocktails</td>
			<td>http://localhost:3000/cocktails</td>
			<td>SearchPage</td>
		</tr>
    <tr>
      <td>See details for a cocktail</td>
      <td>http://localhost:3000/cocktails/detail/{cocktail_id}</td>
      <td>CocktailDetails</td>
		</tr>
      <tr>
      <td>Favorite or Unfavorite a cocktail</td>
      <td>http://localhost:3000/cocktails/detail/{cocktail_id}</td>
      <td>FavoriteToggleButton</td>
		</tr>
    </tr>
      <tr>
      <td>Rate a cocktail</td>
      <td>http://localhost:3000/cocktails/detail/{cocktail_id}</td>
      <td>CocktailDetails</td>
		</tr>
	</tbody>
</table>
<br>


![Back End](images/Splash.jpg)
### Back End

#### Sign Up

| Method | URL | Action |
| ------ | ------ | ------ |
| POST | `http://localhost:8000/docs#/api/accounts` | Create Account |
| GET | `http://localhost:8000/docs#/token` | Get Token |


<details>
<summary><strong>Example Inputs</strong></summary>

##### Create Account:
```
{
  "email": "Lauren@yahoo.com",
  "password": "Laren258",
  "name": "Lauren",
  "birthday": "1/16/1996",
  "city": "Reno",
  "image": "https://t4.ftcdn.net/jpg/03/83/25/83/360_F_383258331_D8imaEMl8Q3lf7EKU2Pi78Cn0R7KkW9o.jpg",
  "experience": "Beginner"
}

```
</details>


#### Log In

| Method | URL | Action |
| ------ | ------ | ------ |
| POST | `http://localhost:8000/docs#/token` | Log In |
| DELETE | `http://localhost:8000/docs#/token` | Log Out |


<details>
<summary><strong>Example Inputs</strong></summary>

##### Create Account:
```
  "email" (This is the username): "Lauren@yahoo.com"
  "password": "Laren258"

```
</details>



#### Cocktails
| Method | URL | Action |
| ------ | ------ | ------ |
| GET | `http://localhost:8000/api/cocktails/` | List all cocktails |
| POST | `http://localhost:8000/api/cocktails/` | Create a cocktail |
| GET | `http://localhost:8000/api/cocktails/cocktail_id/` | Show cocktails details |
| PUT | `http://localhost:8000/api/cocktails/cocktail_id/` | Update a cocktail |
| DELETE | `http://localhost:8100/api/cocktails/cocktail_id/` | Delete a cocktail |

<details>
<summary><strong>Example GET Inputs</strong></summary>

##### List all cocktials:
```
No input required

```
##### Show cocktail details:
```

{
    "cocktail name": "Pina Collada", (string)
    "Alcoholic": True, (bool)
    "Ingredients: ["Rum", "Coconut", "Pineapple"],
    "Image_URL": "https.pinacolada.com", (string)
    "instructions": "Do something to make me a pina colada", (string)
    "Suggested_serving_glass": "angry tiki god", (string)
    "Category": "Fruity", (string)
}

```
</details>

<details>
<summary><strong>Example POST and PUT Input</strong></summary>

```

{
  "cocktail_name": "Margarita",
  "alcoholic": true,
  "ingredients": "Lime Juice",
  "image_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpvRW60gstwMU5MU7w7KA9Qdb8LOS2XAEs7g&usqp=CAU",
  "instructions": "Mix well",
  "suggested_serving_glass": "cocktail glass",
  "category": "cocktail"
}

```

</details>

<details><summary><strong>Example DELETE Input</strong></summary>
<br>

```
enter cocktail id

```
</details>




#### Favorites
| Method | URL | Action |
| ------ | ------ | ------ |
| POST | `http://localhost:8000/api/favorites/` | Create Favorite |
| GET | `http://localhost:8000/api/favorites/` | Get list of favorites |
| DELETE | `http://localhost:8000/api/favorites/favorite_id/` | Delete Favorite |


<details>
<summary><strong>Example GET Inputs</strong></summary>

##### List all favorites:
```
No input required

```

</details>

<details>
<summary><strong>Example POST Input</strong></summary>

```

{
  "cocktail_name": "Margarita"
}

```

</details>

<details><summary><strong>Example DELETE Input</strong></summary>
<br>

```
enter favorite id

```
</details>


#### Ratings
| Method | URL | Action |
| ------ | ------ | ------ |
| GET | `http://localhost:8000/api/ratings/cocktail_id` | Get cocktail ratings|
| GET | `http://localhost:8000/api/cocktail_id/avg` | Get average cocktail ratings |
| GET | `http://localhost:8000/api/cocktail_id/count` | Get cocktail ratings count |
| GET | `http://localhost:8100/api/cocktail_id/user` | Get user rating |
| POST/PUT | `http://localhost:8100/api/cocktail_id/user` | Update or create user rating |


<details>
<summary><strong>Example GET Inputs</strong></summary>



##### Get cocktail ratings:
```
enter cocktail id

```
##### Get average cocktail ratings:
```
enter cocktail id

```
##### Get cocktail ratings count:
```
enter cocktail id

```

##### Get user rating:
```
enter cocktail id

```
</details>

<details>
<summary><strong>Example POST and PUT Input</strong></summary>

```

{
  "review": "This is a great drink that is easy to make",
  "rating": 4
}

```

</details>
