# Daily Journal

D1:

My group and I worked on coming up with an app idea and the wireframe. I proposed two different ideas and we went with another group members choice. After we decided, we then worked on the wireframe. We all worked very well at coming up with ideas and we all shared our thoughts and inputs. We got a lot done today.

D2:

Today we continued to work on our wireframes. We finished early and I asked Riley what to improve on. I told the group we needed to use actual api data in our wireframes and we shared the excalidra and everyone took a piece of the project and updated the wireframe to incorporate the data. We also discussed as a group what to do with the reviews section of our project (where we will add it, and what it will look like). We will continue this discussion tomorrow but today we all worked really well and everyone shared ideas.

D3:

We worked on adding the api routs and the data going in and out for all of our CRUD operations. We all took a part of the project and collaborativelty worked on it. I worked on the edit for drinks.

D4:

Today me and the group updated the api routes and included them in our wireframe where whe had initially put them. We all discussed limitations with our thrid pary api and focusing our attention on a user using CRUD operations instead of putting too much time on using the thrird pary api. That might be a strech goal for us as we focus on the user and CRUD.

D5:

Today I worked on creating issues for each of the features we will be working on for our project. I created the get and get all features for our fastAPI. I will continue to work on these so we can have a to-do list for our project.

**Week 1 Of Group Coding**

D6-5/15/2023:

Our group worked on setting up our project. I helped with setting up the yaml file. We also worked on a git flow so when we merge to main there are no conflicts. Today I shared my screen and coded the create user and get user to make sure that part of the back end would work. For the authentication we are stuck on creating an account. We will wait for the lecture to get more insight on authentication. I spent some time updating and completing the issues we had to create. I also came up with an outline for Tuesdays plan/things to work on. Tomorrow I will ask the group if the play is good or update it if others want changes.

D7-5/16/2023:

I updated the wireframe and I also created the expected in and outs for each crud operation by clearning up the work that we had done as a group. Today we got stuck with an authoirzation error when trying to get the authentication working. My team and I worked all day till the late evening working on getting our authorization feature working. I reivewed all the code we wrote twice and caught some errors when comparing the code to my working sample. We ended up updating all those changes and finally got our authentication feature to work.

D8-5/17/2023:

Today I was very lost with the code. We got many errors throughout our coding session. I am planning on reviewing all the code we wrote today so I have a better understanding of how it works.

D9-5/19/2023:

Today the group and I worked on fixing the update feature which we got to work by the end of the day. We also got the delete feature completed. Our goal for day was me by completing these two features which we dicussed during the morning stand-up.

**Week 2 Of Group Coding**

D10 5/22/2023: Today during the standup the group discussed combinging the differnt backends that were created, ratings and favorites. We then split up into teams to work the a frontend feature. I worked on making the create cocktail form.

D11 5/23/2023: Today the group split into two teams. I helped my team members get the font end authentication up and running. This took us most of project time and navigated through many errors and resources to get it to work.

D12 5/24/2023: Today our group worked on merging all the branches that were made. This took some time as five people’s changes needed to be matched to our dev-branch. We also looked into why our forms were not working since we kept getting auth errors. We were using redux for login and log out but not for anything else. We spent the rest of the day working together to use redux for our cocktail form. In addition, I updated the App.js file and Nav.js file so we could easily navigate.

D13 5/25/2023: Today I worked on changing the list of cocktails code to list of favorites. I was able to get this conversion to work. I spent the rest of the day getting the favorite delete button working.

D14 5/26/2023: Today I spend most of project time working with my partner on the update feature. We needed sier help with feature as there were errors we struggled to get past. We also discussed the bugs briefly with our team members but were not able to get it to work by the end of the day. This feature will continue to be worked on next week.

**Week 3 Of Group Coding**

D15 5/30/2023: Continued to work on update, took a break from this feature. I then worked on redircting pages after creating a cocktail. Today I learned that sometimes you need some other people to review your code to find bugs.

D16 5/31/2023: I decided to work on styling for today with a partner after merging everything. I learned command B brings the file structure out so you can see it.

D17 6/1/2023: Today I worked with the team to get the update button fixed. We were able to get this working now. Also, we fixed the redirects. Then worked on styling. I learned you can drag and drop highlighed code to where you want it.

D18 6/2/2023: Today I updated the ReadMe with all endpoints and instructions. I also worked on styling the create cocktail page. I learned that styling can take just as long as any other part of the project.

**Week 4 Of Group Coding**

D19 6/5/2023: Today, the group and I worked on merging all the projects with the updates that were made. After sucessful mergining, I worked on fixing the backgrounds for all the pages with my partner. I learned that we can create a lot of styling and decorative items in other apps and import them into our project.
