## May 8, 2023

* Not in class: medical issue

## May 9, 2023

* Not in class: medical issue

## May 10, 2023

* Not in class: medical issue

## May 11, 2023

Today, I worked on:

* Worked on back-end

Researched cocktaildb.com, and scraped the website for a decent chunk of their active database to get a good idea of how our database will be structured.

From some comments on github I learned that there are scraping techniques to get database information other than the traditional request method. Some of the data that people pulled isn't available via the known public api endpoints, so I am going to try to figure out how that is done at some point.

## May 12, 2023

Today, I worked on:

* Catching up

Since I was out for medical reasons earlier this week, I am just trying to catch up on the project as a whole. I have a decent idea of how our back-end will look, but I am still having trouble visualizing the front-end.

The wire-frame is a very useful tool. It takes a lot of planning and organization but seems to me to be worth the time and effort.

## May 15, 2023

Today, I worked on:

* Worked as a group on endpoints.

Worked on getting the mongodb set up with docker so we will be able to test endpoints with some data. Mongo is very new to me, but I'm glad we chose it since I already have a small amount of experience with sql from the prior modules. Also started looking at authentication with jwt and fastapi, and how that will affect our back-end code.

## May 16, 2023

Today, I worked on:

* Worked as a group on back-end authentication

We set up the authentication of a user, specifically when they create an account. We also had to modify some of our collection and key names, as well as our docker compose file.

Still confused about how we got the back-end authentication working, it was one of those days where we changed so many different things that I'm not sure exactly when the correct change was made. Thankfully we have a working back-end authentication now for a user creating an account.

## May 17, 2023

Today, I worked on:

* Worked as a group on Model and Query creation, back-end authentication

Almost worked out all of the issues with back-end authentication. Started working on the models, routes and query creation for the cocktails portion of the back-end, but ran into some issues with the query creation and serialization.

Dealing with serialization in this way is tricky, especially considering it's the first time I have had to really wrap my head around BSON and how that differs from JSON.

## May 18, 2023

Today, I worked on:

* Worked as a group on the cocktail route functions

We've built and tested the CRUD endpoints and queries for cocktails. Our code now communicates effectively with the database and all routes to the database are working properly.

I think some of the issues that were throwing me off were due to the object id that is created by MongoDB, as it gets confusing when you are trying to query for a specific object id, but the object id is not a string as it is BSON.

## May 22, 2023

Today, I worked on:

* Started working on Front-end pages

We split up the front-end work based on the amount of pages that needed to be made functional. No authentication yet, just getting the bare bones . I started working on the list page which will show all of the cocktail data, then realized that the search page was where I should start since that is the bulk of the work. I ended up getting the bulk of the search page and list page together, and as of now are probably going to be all on one dynamic page.


Our site is difficult for me to wrap my head around, but I am trying to not go off on a tangent and assume something when working on it, and just stick to the wire-frame as closely as possible.

## May 23, 2023

Today, I worked on:

* Finished Search and List Page

The bulk, or what I thought was the bulk of the search and list page, was finished early in the day, but once I started testing the functionality with the database and how the ui interacts with the user I found that I had to backtrack and fix some of the model relationships. I am very new to any type of front end work, and did not realize how much code it takes just to have some simple functionality like error messages that are used at the right times and on-click interactions.

I get the feeling that this scenario has at least some to do with the unknown-unknown phrase, but I have a lot of trouble sifting through google since any site that sells something is weighted so heavily.

## May 25, 2023

Today, I worked on:

* Worked with group and Trav on front end

Worked as a group on reworking overall layout of the site and some of the smaller functionality components. I also worked with Trav on the card layout for the list that appears on the main list/search page. I then worked on adding a favorites clickable button for the cards, but some of the back-end for that is getting re-worked so I held off.

This is the first time I have been able to sit down and work on the functionality of the front-end page and be able to see errors in the console and have time to read them and see if I can fix them. I'm noticing that some of the warnings are that it is taking a long time for things to render, so it's actually pretty interesting.

## May 26, 2023

Today, I worked on:

* Search/List page

Worked on the Search/List page again today, but didn't have a lot of time since we had a bunch of things today like the social hour and lecture. I also had some computer issues that were 100% self inflicted,  and that put me back quite a bit. Got started on adding the favorites creation functionality to the page via a button, but I am not sure if that is also being done on another page, or if that is also something I need to take care of. The more I think about it the more I realize it is.

I am starting to realize that every time I change something on my page, the error messaging is also going to get altered, at least with what I am doing since it's still being built.

## May 30, 2023

* Search/List page

Continued to struggle with the search/list page. I worked on it over the weekend as well, because I have no life and these things bug me, and was struggling to find a way to implement a favorites button that would toggle between create and delete on click and would update itself and work fluidly?  Since the page will be listing all of the cocktails regardless of whether or not  the user is signed in, I would have A button created and implemented into the page in the same file, but either I could see the page in general when I was logged in, or when I was not, depending on what I tried. I had moved the button to it's own page just for the sake of organization, and to test it, and then had what was a big brain moment, for me, Monday night, where I would keep the button component separated and just not render it when the list is creating. It seems really obvious and the only solution I can still think of that's logical, but I just haven't had to do any type of problem solving like this with coding before. In my past work everyday, but this is just so new to me.

## May 31, 2023

Today, I worked on:

* Authentication

Worked on making sure there weren't any authentication issues on several pages.

I learned that redux makes authentication a lot easier, especially once you can wrap your head around using the endpoints with rtk query.

## June 1, 2023

Today, I worked on:

* Styling with Trav, removed unneeded elements

Worked on styling the website with Trav, tested out different css classes on different elements. I also ended up removing the search button as it wasn't really necessary. The search bar actively filters the list, and the dropdown automatically filter when selected now.

Filtering without the need for a button, is called 'live search' or 'instant search'.

## June 2, 2023

Today, I worked on:

* Styling by myself and as a group.

Worked on styling, planned out for the most part what we all would be working on, got a little bit of work done but had class for a chunk of the day. All pulled and pushed to git so we have a good working copy for the weekend.

Today, I found out that if you don't pay attention and try to merge your branch on gitlab, you will most likely merge it with main. Thankfully it didn't go through because we don't push everything to main, so it was behind a bit and wouldn't do it.

## June 5, 2023

Today, I worked on:

* Finishing up the project.

We worked as a group merging what we had finished over the weekend. We also worked on making sure the project is uniform. I am also trying to figure out how to show an error message directly above my combined search bar/form drop-downs. I know I had figured this out a long time ago, but it has changed so much, and what I had done previously won't work, so technically I haven't figured this out before. A good chunk of the day was spent taking the practice exam.

## June 6, 2023

Today, I worked on:

* Worked as a group wrapping up the project

We pushed anything we had worked on the night before. After lunch we went through the entire project, deleting console logs, old comments and organized the files within the project directory.

You can use an _ for a variable, not sure if you are supposed to, but you can.
