# Project Journal

D1:

Today we came up with several ideas for a project before finally settling on a cocktail app. I think the Cocktail app idea was one of Jasmine's.
We spent a good bit of time making a wire frame for the app. I think that we got a lot done today.

I leaned that you can share blocks of code in slack by using ``` before pastin in the code.I think that this will be useful during our project.


D2:

Our group finished the wire frame for the cocktail app today. We added in the planned api endpoints that we had decided on. We added these to a google doc and decided on how they should be structured and what we would need.


D3:

Today we took a part of the project and planned out what functionality we would need for each bit and how we would access the endpoints for this.


D4:
We discussed not using a 3rd party api today. The API that we had found is too limiting for us to use. It has a limit on the number of calls / and cocktails that you can request.
We decided to focus on making our app usable and will keep the use of an api in reserve incase we feel we need something else.
We added the CRUN functionality to the wireframe.


D5:

We discussed the project and how we would tackle the tasks today. We decided on a work flow and what steps we would need to address in what order.
Our group voted to use MongoDB for the database in our project, so a good bit of reading about that was done.
I learnt the difference between a relational and non-relational database today.

# Week 1

D6-5/15/2023:


15/05/2023

Worked well as a team today, we started off with deciding our git flow for the project and outlining steps that we wanted to follow. We took it in turns to code with the other team members watching and picking up on mistakes.

Today I discovered that you can ammend multiple lines of text by placing the cursor where you want to type and holding the option key.

Today we managed to:
-make endpoints for users in swagger
-Create a login/out/create user method
-Set up and link the database to our project
-amend the yaml file.

-We got caught on the verification whilst being logged in.

D7-5/16/2023:

We got a good bit of our backend up and working yesterday, today was spent working on applying authorization to the endpoints. I can see how this will be tricky to handle in React over the next week or so.

Today I learned that you can expand and collapse the file sidebar in VS code by hitting command B

D8-5/17/2023:

Authorization and coding in a new backend framework got the better of us somewhat today. I think that we'll have a working backend by tomorrow.

Today I learned that you can use the command F for find, to also replace searched for words.

D9-5/19/2023:

We worked as a group today to get our backend finished. We hit the update feature and delete (which were causing some trouble) and got it all working by the end of the day.

Today I learned a lot about searching Docker container logs to trouble shoot errors in the backend code.

# Week 2

D10 5/22/2023:

In this mornings  standup we discussed combining different back ends that were doing very similar tasks. We then split up into small groups to tackle our front end. We are learning about handling authentication on the front end in tomorrow's lecture, so we voted to get the front end up and working and add in the authentication later.
I got the sign up page up in a rough draft.

Today I learnt that I really enjoy working with api objects in React.

D11 5/23/2023:

Authentication hit us hard today. It ate up all of the project time for the day. We got a couple of pages that didn't need authentication so much to work, and managed to have a functioning sign up page.
I think that we may have to learn and implement REDUX to handle the JWT token from a central store.

D12 5/24/2023:

We spent a long time merging all files from all users into a central branch today, the end product was a basic app. We still have a lot of auth errors. We used Redux to handle the Auth on the app, but didn't implement for other things. TOwards the end of the day we did start to transfer other features' state over to Redux, hoping this will give us more control.

I got the login and sign up pages working today.

Today I learned that holding the state centrally in Redux was not as intimidating as it first seemed. It definitely works differently though, and we'll have to learn more about it.


D13 5/25/2023:

Today I got stuck for a long time on managing multiple states in REDUX in one component. If I can't find a solution tomorrow I'll have to seek help. It's a problem that our whole group has found, but we're yet to find a solution between us.
I have been working on the profile/favourites page of our app.

D14 5/26/2023:

I caved in today and sought help for managing multiple states. It turns out that you can re-assign the isLoading feature baked into Redux as another variable and then assess the completeness of the call in an if statement using the multiple variables.
Every day is a learning day!
I'll not forget this one in a hurry. With that knowledge, I was able to get the page up and running and give assistance on other pages within our group.
This project is really starting to be fleshed out.

# Week 3

D15 5/30/2023:

I managed to complete the Profile page and get the favorites up and showing for the signed uin user. I also managed to add some temporary styling to the pages I'm working on to make it easier to see the information getting used. I did have a little trouble implementing the same card structure as on the other pages, I guess we'll tackle styling as a team though to leave a more unified design.

Today I learned that you can drag the cursor up from the bottom of the VS code window to open a terminal.

D16 5/31/2023:

Today was spent tinkering on the styling of the pages that i have made, helping others with bugs, getting things merged, and refining the navigation so that we can all get around our app easier. I implemented more structure to the React Router routes.

Today I re learned nested routes in react router. I think I'll have to refer to the structure of this a few times before it sinks in.

D17 6/1/2023:

Today we found we were still having issues with the Update page on the front end. I worked with jasmine and Jordan to get the Update and Delete functions working. We also implemented some automatic redirects on certain functions throughout the app.
One of the issues we are having is getting a couple of pages to update upon actions being completed on other pages. They are updating, but often need a refresh of the page for the changes to be iumplemented.

Today I learned about the useNavigate() function with react router. In previous projects I have used a windowredirect function. This seems a bit neater.

D18 6/2/2023:

Today we got a MVP up and running, the app is functional but does need a few tweaks to styling and the linting errors addressing. I realised at this point, I have very little style and was thankful for Trav and Jasmine take the lead on design.

Today I remembered I could install black to handle a good amount of the linting errors. I took care of the rest manually. As I'm not great at design, I thought this would be the best way I could help the team.

# Week 4

D19 6/5/2023:

It felt like a tough day today, having sat the exam this morning I was pretty wiped out. We managed to go over git and make sure that all the little bits we had done over the weekend went together without much trouble. It's really looking like a finished product right now.

I can really see the importance of pushing small changes regularly to make sure you're never too far away from the source code after today's git session. We've really handled it all quite well throught this project though.

D20 6/5/2023:

Today we got the app finished and to a point where we are happy to hand it in. The design that Trav and Jasmine had us stick to looks really good. We got all merged in a final push and re-addressed the linting errors we had created, then removed all print statements. Last step now is to push our journal entries.


D21 6/6/2023:

We're all wrapped up with the project. It's looking good and very usable. Today we updated the login page to have a link to signup. It took us some time to find the way to make a left border visable in Bootstrap. It turns out that the border-left class has been changed to border-start in Boootstrap 5.


