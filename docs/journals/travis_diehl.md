## Trav Journals for Bar Buddy

## May 8th, 2023
* Worked on Wireframing and Site Design

Spent some time researching what professional wireframing looks like.  Our first design we whipped through very quickly, but were asked to add more detail--- very specific details.  It was an exciting time to imagine all the things we could do with our website.  Including adding an api.  Thinking of doing a cocktail website, or perhaps a travel website, or something with birds.

## May 9th, 2023
* Continued to flesh out design, planned methods

We did a lot of researching figuring out what kind of apis we could use that would work best, and it proved surprisingly difficult to find good, free ones.  So we are toying with the idea of scrapping the api all together and making it be a self-data website.  A portfolio of your own cocktails.

## May 10th, 2023
* Continued to flesh out wireframe, added detail to the endpoints

We finally decided to keep things simple and ditch the api.   Wireframe is ready and we mostly worked on getting the details on our endpoints and how we want to start implementing the backend.  My teammates are so smart!

## May 11th, 2023
* Completed Endpoints

Got all the endpoints and details figured out, now we are mostly waiting on some base coide to work with.

## May 12th, 2023
* Completed Design, mostly waiting for coding time

Started investigating what would be better to use Mongo or SQL.  The team seems divided but in a way where we all just kinda go along with eachother haha.

## May 15th, 2023
* Set up Mongo Db, get and create user, signup

We settled on Mongo DB and spent the day exploring how that works, beginning to start working on our CRUD.

## May 16th, 2023
* Added in authenticator and accounts, cleaned files

Did a lot of watching today, I still feel like I'm fuzzy with a lot of the backend work, so it's been nice learning alongside my teammates, but I do wish we had had personal time to work with the material before being in groups because I will not be slowing down our progress just to ask a million questions.  Will have to take another class on Mongo and FASTapi another time.

## May 17th, 2023
* Continued working on CRUD applications

Got the authorization working after hours of debugging.  Spent whole day debugging.

## May 22nd, 2023
* Ratings added, started working on seperate pages, worked on the log in and home page and nav bar

-established endpoints in the router files

-established models and their functions to work with the endpoints

-almost done with backend!

-spent a lot of time watching Jordan work, he walked us through much of the code and we came up with bugging solutions together.

-hit another internal server error and strugglin

## May 23rd, 2023
* Working with the team to fix redux issues, worked on login

-we worked on indivdual items of the project.  each seperating into different pages.  I was nervous of this because it’s been awhile since I’ve worked with react alone and I still don’t feel very confident in it.  But it was good to dive in again.  I got a good framework for the Home page, nav component and login page.   Though I was unable to test them due to not having a full App.js, which we agreed to handle together later.


## May 24th, 2023
* worked with redux on fetching data

Another day of me mostly watching and chiming in on spelling errors or little mistakes.  There are so many languages and frameworks we are implementing at once, it is very hard to keep track.

## May 25th, 2023
* completed login page and home page for now.  Worked with jasmine and others on getting login to work.

--Struggling with some git issues, I was very frusterated at the lack of information in the terminal errors. Eventually was fixed with some help from Micheal.

-Upon discovery of new errors, at first thought it was a CORS error, it ended up being solved by switching some of our material to react redux.  We then decided to change the project in general to all using redux, while before we had only had Login and Logout using that library.

## May 30st, 2023
* Working on design, added Flexbox and continued to discover bootstrap

-team split up into groups and I finally found my comfort zone of visuals and css.  Was definately more nerwracking because CSS effects everyones code as well as my own, but very rewarding to see things come together.

## June 1st, 2023
* Spent the day working on my unit test

-learned that 'get' is the easiest thing to test, as when you are working with delete or create you have to make multiple sets of tests and fake data in order for it to test itself. Worked with several seirs and instructors over hours to get this done.

## June 2nd, 2023
* Design Day

-Continued working on the CSS and the overall design.  We will have to make some major changes to our wireframe, as a lot of what we had planned is hard to find on the internet-- such as certain image styles, etc.  I am having a lot of fun with the styling.

## June 5th, 2023
* All backend fully completed, bugs worked out.  The group worked on merging the styles together.

-took the lead on the styling and it felt good to be helpful and get my creative side going.  On track to finish the whole project very soon.
-spent a lot of the day merging and working with git.

## June 6th, 2023
* Basically done with the project, we are dedicating the day to finishing up the housekeeping and making sure all of our files are the same.