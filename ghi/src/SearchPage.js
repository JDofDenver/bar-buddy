import React, { useState, useEffect } from "react";
import { useGetAccountQuery } from "./app/cocktailSlice";
import FavoriteToggleButton from "./FavoriteToggleButton";
import { Link } from "react-router-dom";
import "./App.css";


function SearchPage() {
	const [cocktails, setCocktails] = useState([]);
	const [selectedOption, setSelectedOption] = useState(null);
	const [searchTerm, setSearchTerm] = useState("");
	const [selectedIngredient, setSelectedIngredient] = useState("");
	const [selectedCategory, setSelectedCategory] = useState("");
	const [filteredCocktails, setFilteredCocktails] = useState([]);
	const [error, setError] = useState(null);
	const [hasSearched, setHasSearched] = useState(false);
	const { data: account } = useGetAccountQuery();
	const [noResults, setNoResults] = useState(false);

	useEffect(() => {
		const fetchCocktails = async () => {
			try {
				const response = await fetch("http://localhost:8000/api/cocktails/");
				if (!response.ok) {
					throw new Error("Something went wrong!");
				}
				const data = await response.json();
				setCocktails(data.cocktails);
				setFilteredCocktails(data.cocktails);
			} catch (error) {
				setError(error.message);
				window.alert(error.message);
			}
		};
		fetchCocktails();
	}, []);

	useEffect(() => {
		if (hasSearched) {
			let filteredCocktails = [];
			if (selectedOption === "cocktailName" && searchTerm !== "") {
				filteredCocktails = cocktails.filter((cocktail) => cocktail.cocktail_name.toLowerCase().includes(searchTerm.toLowerCase()));
			} else if (selectedOption === "ingredient" && selectedIngredient !== "") {
				filteredCocktails = cocktails.filter((cocktail) =>
					cocktail.ingredients
						.toLowerCase()
						.split(",")
						.map((ingredient) => ingredient.trim())
						.includes(selectedIngredient.toLowerCase())
				);
			} else if (selectedOption === "category" && selectedCategory !== "") {
				filteredCocktails = cocktails.filter((cocktail) => cocktail.category === selectedCategory);
			} else {
				filteredCocktails = cocktails;
			}
			if (filteredCocktails.length === 0) {
				setNoResults(true);
			} else {
				setNoResults(false);
				setFilteredCocktails(filteredCocktails);
			}
		}
	}, [selectedOption, searchTerm, selectedIngredient, selectedCategory, cocktails, hasSearched]);

	const handleOptionClick = (option) => {
		setSelectedOption(option);
		setHasSearched(false);
		setNoResults(false);
	};

	const handleSearchTermChange = (e) => {
		setSearchTerm(e.target.value);
		handleSearch();
	};

	const handleIngredientChange = (e) => {
		setSelectedIngredient(e.target.value);
		handleSearch();
	};

	const handleCategoryChange = (e) => {
		setSelectedCategory(e.target.value);
		handleSearch();
	};

	const handleSearch = () => {
		setError("");
		setHasSearched(true);
	};

	const handleSearchTermKeyDown = (e) => {
		if (e.key === "Enter") {
			handleSearch();
		}
	};

	const renderSearchOptions = () => {
		const isCocktailNameSelected = selectedOption === "cocktailName";
		const isIngredientSelected = selectedOption === "ingredient";
		const isCategorySelected = selectedOption === "category";

		return (
			<div className="search-image-buttons-container">
				<div
					className={`searchPageBoxStyle ${isCocktailNameSelected ? "selected-box" : ""}`}
					onClick={() => handleOptionClick("cocktailName")}>
					<span className="searchPageSpanStyle"></span>
					<img src={process.env.PUBLIC_URL + "/search-by-name.jpg"} alt="batchone" className="searchPageImageStyle" />
				</div>

				<div
					className={`searchPageBoxStyle ${isIngredientSelected ? "selected-box" : ""}`}
					onClick={() => handleOptionClick("ingredient")}>
					<span className="searchPageSpanStyle"></span>
					<img src={process.env.PUBLIC_URL + "/search-by-ingredient.jpg"} alt="batchtwo" className="searchPageImageStyle" />
				</div>

				<div
					className={`searchPageBoxStyle ${isCategorySelected ? "selected-box" : ""}`}
					onClick={() => handleOptionClick("category")}>
					<span className="searchPageSpanStyle"></span>
					<img src={process.env.PUBLIC_URL + "/search-by-category.jpg"} alt="batchthree" className="searchPageImageStyle" />
				</div>
			</div>
		);
	};

	const renderSearchForm = () => {
		if (selectedOption === "cocktailName") {
			return (
				<div className="searchBar">
					<input
						type="text"
						value={searchTerm}
						onChange={handleSearchTermChange}
						onKeyDown={handleSearchTermKeyDown}
						placeholder="Search by Cocktail Name..."
						autoComplete="off"
						className="searchInput"
					/>
				</div>
			);
		} else if (selectedOption === "ingredient") {
			return (
				<div className="searchBar">
					<select className="searchBarInputSelect" value={selectedIngredient} onChange={handleIngredientChange}>
						<option value="">Select an Ingredient</option>
						<option value="Whiskey">Whiskey</option>
						<option value="Rum">Rum</option>
						<option value="Gin">Gin</option>
						<option value="Vodka">Vodka</option>
						<option value="Mocktail">Mocktail</option>
						<option value="Liqueur">Liqueur</option>
					</select>
				</div>
			);
		} else if (selectedOption === "category") {
			return (
				<div className="searchBar">
					<select className="searchBarInputSelect" value={selectedCategory} onChange={handleCategoryChange}>
						<option value="">Select a Category</option>
						<option value="ordinary drink">Ordinary Drink</option>
						<option value="cocktail">Cocktail</option>
						<option value="shake">Shake</option>
						<option value="shot">Shot</option>
						<option value="coffee or tea">Coffee or Tea</option>
						<option value="other">Other</option>
					</select>
				</div>
			);
		} else {
			return null;
		}
	};
	const renderResults = () => {
		const cocktailsToRender = filteredCocktails;
		if (noResults) {
			return (
				<div className="search-results-container">
					<p>No results found. Log in and create your own!</p>
				</div>
			);
		}
		if (cocktailsToRender.length > 0) {
			return (
				<div className="search-cards-container">
					{cocktailsToRender.map((cocktail, index) => {
						const cardContent = (
							<div className="search-card-container">
								<img src={cocktail.image_url} alt={cocktail.cocktail_name} />
								<div>
									<h3>{cocktail.cocktail_name}</h3>
									<p>{cocktail.category}</p>
									{account && (
										<div className="favorite-button">
											<FavoriteToggleButton cocktail={cocktail} />
										</div>
									)}
								</div>
							</div>
						);

						return (
							<div key={index}>
								{account ? <Link to={`/cocktails/${cocktail.id}`}>{cardContent}</Link> : <div>{cardContent}</div>}
							</div>
						);
					})}
				</div>
			);
		}
	};

	return (
		<div>
			<br></br>
			<center>
				<div className="search-card-container">
					<h1>Pick your Poison!</h1>
				</div>
			</center>
			<br></br>
			{renderSearchOptions()}
			{renderSearchForm()}
			{renderResults()}
		</div>
	);
}

export default SearchPage;
