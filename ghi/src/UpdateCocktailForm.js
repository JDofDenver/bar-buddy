
import React, { useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useGetUpdateMutation } from './app/cocktailSlice';
import { useDispatch } from 'react-redux'


function UpdateCocktailForm() {
    const { cocktail_id } = useParams();
    const [cocktailName, setCocktailName] = useState('');
    const [alcoholic, setAlcoholic] = useState();
    const [ingredients, setIngredients] = useState('');
    const [image, setImage] = useState('');
    const [instructions, setInstructions] = useState('');
    const [servingGlass, setServingGlass] = useState('');
    const [category, setCategory] = useState('');
    const navigate = useNavigate();

    const [getUpdate] = useGetUpdateMutation();

    const dispatch = useDispatch();

    const handleSubmit = async (event) => {
        event.preventDefault();
        const updatedCocktail = {
            cocktail_name: cocktailName,
            alcoholic: alcoholic,
            ingredients: ingredients,
            image_url: image,
            instructions: instructions,
            suggested_serving_glass: servingGlass,
            category: category
        };
        await getUpdate({cocktail_id: cocktail_id, body: updatedCocktail})

        navigate(`/cocktails/${cocktail_id}`)

    };

    const handleCocktailNameChange = (event) => {
        setCocktailName(event.target.value);
    };

    const handleAlcoholicChange = (event) => {
        setAlcoholic(event.target.value);
    };

    const handleIngredientChange = (event) => {
        setIngredients(event.target.value);
    };

    const handleImageChange = (event) => {
        setImage(event.target.value);
    };

    const handleInstructionChange = (event) => {
        setInstructions(event.target.value);
    };

    const handleServingGlassChange = (event) => {
        setServingGlass(event.target.value);
    };

    const handleCategoryChange = (event) => {
        setCategory(event.target.value);
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Update Cocktail</h1>
                    <form onSubmit={handleSubmit} id="update-cocktail-form">
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleCocktailNameChange}
                                value={cocktailName}
                                name="cocktailName"
                                placeholder="Cocktail Name"
                                required
                                type="text"
                                id="cocktailName"
                                className="form-control"
                            />
                            <label htmlFor="cocktailName">Cocktail Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select
                                value={alcoholic}
                                onChange={handleAlcoholicChange}
                                required
                                type="text"
                                name="alcoholic"
                                id="alcoholic"
                                className="form-control"
                            >
                                <option value="">Select Option</option>
                                <option value="true">Alcoholic</option>
                                <option value="false">Non-Alcoholic</option>
                            </select>
                            <label htmlFor="alcoholic">Alcoholic?</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleIngredientChange}
                                value={ingredients}
                                name="ingredients"
                                placeholder="ingredients"
                                required
                                type="text"
                                id="ingredients"
                                className="form-control"
                            />
                            <label htmlFor="ingredients">Ingredients</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleImageChange}
                                value={image}
                                name="image"
                                placeholder="image"
                                required
                                type="text"
                                id="image"
                                className="form-control"
                            />
                            <label htmlFor="image">Image</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleInstructionChange}
                                value={instructions}
                                name="instructions"
                                placeholder="instructions"
                                required
                                type="text"
                                id="instructions"
                                className="form-control"
                            />
                            <label htmlFor="instructions">Instructions</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleServingGlassChange}
                                value={servingGlass}
                                name="servingGlass"
                                placeholder="servingGlass"
                                required
                                type="text"
                                id="servingGlass"
                                className="form-control"
                            />
                            <label htmlFor="servingGlass">Serving Glass</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select
                                value={category}
                                onChange={handleCategoryChange}
                                placeholder="category"
                                required
                                type="text"
                                name="category"
                                id="category"
                                className="form-control"
                            >
                                <option value="">Choose a Category</option>
                                <option value="ordinary drink">Ordinary Drink</option>
                                <option value="cocktail">Cocktail</option>
                                <option value="shake">Shake</option>
                                <option value="shot">Shot</option>
                                <option value="coffee or tea">Coffee or Tea</option>
                                <option value="other">Other</option>
                            </select>
                            <label htmlFor="category">Category</label>
                        </div>
                        <button className="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default UpdateCocktailForm;
