import { createSlice, createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const accountsApi = createApi({
	reducerPath: "accountsApi",
	baseQuery: fetchBaseQuery({
		baseUrl: "http://localhost:8000/",
		provideTags: ["Account"],
	}),

	endpoints: (builder) => ({
		getCocktailRatings: builder.query({
			query: (cocktail_id) => ({
				url: `/api/ratings/${cocktail_id}`,
				credentials: "include",
			}),
			providesTags: ["Ratings"],
		}),
		getAvgRatings: builder.query({
			query: (cocktail_id) => ({
				url: `/api/ratings/${cocktail_id}/avg`,
				credentials: "include",
			}),
			providesTags: ["Ratings"],
		}),
		getOneUserRating: builder.query({
			query: (cocktail_id) => ({
				url: `/api/ratings/${cocktail_id}/user`,
				credentials: "include",
			}),
			providesTags: ["Ratings"],
		}),
		getRatingsCount: builder.query({
			query: (cocktail_id) => ({
				url: `/api/ratings/${cocktail_id}/count`,
				credentials: "include",
			}),
			providesTags: ["Ratings"],
		}),
		updateOrCreateRating: builder.mutation({
			query: (data) => {
				const ratingURL = `/api/ratings/${data.cocktail_id}/user`;
				const body = { rating: data.data.rating, review: data.data.review };
				return {
					url: ratingURL,
					body: body,
					method: "POST",
					credentials: "include",
				};
			},
			invalidatesTags: ["Ratings"],
		}),
		createFavorites: builder.mutation({
			query: (body) => ({
				url: "/api/favorites/",
				body: body,
				method: "POST",
				credentials: "include",
			}),
			invalidatesTags: ["Favorite"],
		}),
		listFavorites: builder.query({
			query: () => ({
				url: "/api/favorites",
				credentials: "include",
			}),
			providesTags: ["Favorite"],
		}),
		deleteFavorite: builder.mutation({
			query: (favorite_id) => ({
				url: `/api/favorites/${favorite_id}`,
				method: "DELETE",
				credentials: "include",
			}),
			invalidatesTags: ["Favorite"],
		}),
		getProfile: builder.query({
			query: () => ({
				url: "/api/account",
				credentials: "include",
			}),
			providesTags: ["Profile"],
		}),
		createAccount: builder.mutation({
			query: (body) => ({
				url: "/api/accounts",
				body: body,
				method: "POST",
				credentials: "include",
			}),
			invalidatesTags: ["Account"],
		}),
		getAccount: builder.query({
			query: () => ({
				url: "/token",
				credentials: "include",
			}),
			transformResponse: (response) => response || null,
			providesTags: ["Account"],
		}),
		logout: builder.mutation({
			query: () => ({
				url: "/token",
				method: "DELETE",
				credentials: "include",
			}),
			invalidatesTags: ["Account"],
		}),
		login: builder.mutation({
			query: ({ username, password }) => {
				const body = new FormData();
				body.append("username", username);
				body.append("password", password);
				return {
					url: "/token",
					method: "POST",
					body,
					credentials: "include",
				};
			},
			invalidatesTags: ["Account"],
		}),
		cocktailsNew: builder.mutation({
			query: (body) => ({
				url: "/api/cocktails/",
				body: body,
				method: "post",
				credentials: "include",
			}),
			providesTags: ["Cocktails"],
		}),
		getCocktailDetails: builder.query({
			query: (cocktail_id) => ({
				url: `/api/cocktails/${cocktail_id}`,
				credentials: "include",
			}),
			providesTags: ["Cocktails"],
		}),
		getCocktailList: builder.query({
			query: () => ({
				url: "/api/cocktails",
				credentials: "include",
			}),
			providesTags: ["Cocktails"],
		}),
		getDelete: builder.mutation({
			query: (cocktail_id) => ({
				url: `/api/cocktails/${cocktail_id}`,
				method: "DELETE",
				credentials: "include",
			}),
			invalidatesTags: ["Cocktails"],
		}),
		getUpdate: builder.mutation({
			query: (data) => {
				const CocktailURL = `/api/cocktails/${data.cocktail_id}`;
				const updateData = data.body;
				return {
					url: CocktailURL,
					body: updateData,
					method: "PUT",
					credentials: "include",
				};
			},
			invalidatesTags: ["Cocktails"],
		}),
	}),
});

export const {
	useLogoutMutation,
	useLoginMutation,
	useGetAccountQuery,
	useCreateAccountMutation,
	useGetProfileQuery,
	useGetCocktailDetailsQuery,
	useCocktailsNewMutation,
	useGetCocktailListQuery,
	useGetDeleteMutation,
	useGetUpdateMutation,
	useListFavoritesQuery,
	useCreateFavoritesMutation,
	useDeleteFavoriteMutation,
	useGetOneUserRatingQuery,
	useGetCocktailRatingsQuery,
	useGetAvgRatingsQuery,
	useGetRatingsCountQuery,
	useUpdateOrCreateRatingMutation,
} = accountsApi;

export default accountsApi.reducer;
