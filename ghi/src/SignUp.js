import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom"

function SignUp() {
  const [birthday, setBirthday] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [city, setCity] = useState("");
  const [experience, setExperience] = useState("");
  const [image, setImage] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.email = email;
    data.password = password;
    data.name = name;
    data.birthday = birthday;
    data.city = city;
    data.image = image;
    data.experience = experience;



    const makeAccountUrl = "http://localhost:8000/api/accounts";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(makeAccountUrl, fetchConfig);
    if (response.ok) {
      const knew = await response.json();
      navigate("/login");
    }
  };

  const handleBirthdayChange = (event) => {
    const value = event.target.value;
    setBirthday(value);
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  };

  const handleCityChange = (event) => {
    const value = event.target.value;
    setCity(value);
  };

  const handleExperienceChange = (event) => {
    const value = event.target.value;
    setExperience(value);
  };

const handleImageChange = (event) => {
    const value = event.target.value;
    setImage(value);
};

const handlePasswordChange = (event) => {
    const value = event.target.value;
    setPassword(value);
}

  useEffect(() => {
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Sign Up</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <input
                value={name}
                onChange={handleNameChange}
                placheholder="name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={birthday}
                onChange={handleBirthdayChange}
                placeholder="D.O.B."
                required
                type="date"
                name="dateTime"
                id="dateTime"
                className="form-control"
              />
              <label htmlFor="dateTime">D.O.B.</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={email}
                onChange={handleEmailChange}
                placeholder="email"
                required
                type="email"
                name="email"
                id="email"
                className="form-control"
              />
              <label htmlFor="email">Email</label>
            </div>

            <div className="form-floating mb-3">
              <input
                value={city}
                onChange={handleCityChange}
                placeholder="city"
                required
                type="text"
                name="city"
                id="city"
                className="form-control"
              />
              <label htmlFor="city">City</label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={experience}
                onChange={handleExperienceChange}
                placeholder="experience"
                required
                type="text"
                name="experience"
                id="experience"
                className="form-control"
              >
                <option></option>
                <option value="beginner">Beginner</option>
                <option value="intermediate">Intermediate</option>
                <option value="pro">Pro</option>
              </select>
              <label htmlFor="experience">Select experience level</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={image}
                onChange={handleImageChange}
                placeholder="image"
                type="text"
                name="image"
                id="image"
                className="form-control"
              />
              <label htmlFor="image">Upload an image URL</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={password}
                onChange={handlePasswordChange}
                placeholder="password"
                required
                type="password"
                name="password"
                id="password"
                className="form-control"
              />
              <label htmlFor="password">Password</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default SignUp;
