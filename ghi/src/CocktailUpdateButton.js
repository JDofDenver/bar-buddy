import React from 'react';
import { useGetUpdateMutation } from './app/cocktailSlice';


const CocktailUpdateButton = (props) => {
    console.log(props.props)

    const [getUpdate] = useGetUpdateMutation();

    const handleUpdate = async () => {
        try {
            getUpdate(props.props);
        } catch (error) {
            console.log('Error updating cocktail:', error);
        }
    };

    return (
        <button className="btn btn-danger" onClick={handleUpdate}>
            Update
        </button>
    );
};

export default CocktailUpdateButton;
