function RatingSetter(stars, starRate) {
    let newStars = stars
    let starBool = true;
    let len = Object.keys(newStars).length

    for (let i = 1; i <= len; i++) {
        newStars[i.toString()] = starBool
        if (i == starRate) {
            starBool = false;
        }
    }
    return newStars
}

export default RatingSetter;
