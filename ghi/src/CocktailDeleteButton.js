
import React from 'react';
import { useGetDeleteMutation } from './app/cocktailSlice';
import {useNavigate} from "react-router-dom"


const CocktailDeleteButton = (props) => {

    const [getDelete] = useGetDeleteMutation();
    const navigate = useNavigate();
    const handleDelete = async () => {
        try {
            getDelete(props.props);
            navigate("/cocktails")
        } catch (error) {
            console.log('Error deleting cocktail:', error);
        }
    };

    return (
        <button className="btn-no" onClick={handleDelete}>
            Delete
        </button>
    );
};

export default CocktailDeleteButton;
