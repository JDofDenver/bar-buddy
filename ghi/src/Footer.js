import React from "react";
import gitlabLogo from "./images/gitlab_logo.svg";

const Footer = () => {

    const team = ["Michael Marquart", "Jasmine Bagha", "Jordan Aguirre", "Trav Diehl", "John Dawson"];

    return (
        <div className="container">
            <footer style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: '1rem', borderTop: '1px solid #ddd'}}>
                <div style={{ display: 'flex', gap: '1rem' }}>
                    {team.map((member, index) => (
                        <span key={index} className="mb-3 mb-md-0 mx-2 text-muted">{member}</span>
                    ))}
                </div>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <a href="https://gitlab.com/tamarins/module3-project-gamma">
                        <img src={gitlabLogo} alt="GitLab" style={{ width: "30px", height: "30px" }} />
                    </a>
                    <span>    Gitlab.com</span>
                </div>
            </footer>
        </div>
    );
};

export default Footer;
