import { useGetAccountQuery, useListFavoritesQuery, useGetCocktailListQuery} from "./app/cocktailSlice";
import {Link} from "react-router-dom"
import './App.css';


function ProfileTemplate() {
    const { data: profile, isLoading: profileLoading } = useGetAccountQuery();
    const { data: favorites, isLoading: favoritesLoading } = useListFavoritesQuery();
    const { data: list, isLoading: cocktailsLoading } = useGetCocktailListQuery();


    if (profileLoading || favoritesLoading || cocktailsLoading) {
        return <div>Loading...</div>;
    }
    const myFavorites = favorites.favorites.map((favorite) => favorite.cocktail_name);


        return (
            <>
            <div className="container p-0 container-io justify-content-center shaddow">
            <div className="card card-io justify-content-center">
                <div className="row">
                    <div className="col">
                        <div className="d-flex justify-content-center img">
                            <img src={profile.account.image} alt="" className="img-rounded" />
                        </div>
                    </div>
                    <div className="col-md-6 details">
                        <div className="d-flex text-center details">
                        <blockquote>
                            <h5>{profile.account.name}</h5>
                            <small><cite title="Source Title">{profile.account.city}<i className="icon-map-marker"></i></cite></small>
                        </blockquote>
                        </div>
                        <div>
                                <p>
                                    Email :  {profile.account.email} <br/>
                                    Experience level :  {profile.account.experience} <br/>
                                    D.O.B. :  {profile.account.birthday}
                                </p>
                            </div>
                    </div>
                </div>
            </div>
            </div>

                <div className="row">
                    <br />
                </div>
                <div className="text-center">
                    <h1>{myFavorites.length > 0 && "My Favorites"|| "You have no favorite cocktails"}</h1>
                </div>

                <div className="search-cards-container">
                    {list.cocktails?.filter((cocktail) => myFavorites.includes(cocktail.cocktail_name)).map((cocktail, index) => {
                        const cardContent = (
                            <div className="search-card-container">
                                <img src={cocktail.image_url} alt={cocktail.cocktail_name} />
                                <div>
                                    <h3>{cocktail.cocktail_name}</h3>
                                    <p>{cocktail.category}</p>
                                </div>
                            </div>
                        );

                        return (
                            <div key={index}>
                                <Link to={`/cocktails/${cocktail.id}`}>
                                    {cardContent}
                                </Link>
                            </div>
                        );
                    })}
                </div>
            </>

            )
};





export default ProfileTemplate;
