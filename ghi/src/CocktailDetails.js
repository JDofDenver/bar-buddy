import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import {
    useGetAccountQuery,
    useGetCocktailDetailsQuery,
    useGetCocktailRatingsQuery,
    useGetAvgRatingsQuery,
    useGetRatingsCountQuery,
    useGetOneUserRatingQuery,
    useUpdateOrCreateRatingMutation,
} from "./app/cocktailSlice";
import { useNavigate } from "react-router-dom";
import CocktailDeleteButton from "./CocktailDeleteButton";
import FavoriteToggleButton from './FavoriteToggleButton';
import { ReactComponent as StarIcon } from './images/star.svg';
import { ReactComponent as StarFilledIcon } from './images/star-filled.svg'
import RatingSetter from "./RatingSetter";


function CocktailDetails() {
    const
        navigate = useNavigate();

    const
        {
            cocktail_id
        } = useParams();

    const
        {
            data: account,
            isLoading: accountLoading
        } = useGetAccountQuery();

    const
        {
            data: cocktail,
            isLoading: cocktailLoading
        } = useGetCocktailDetailsQuery(cocktail_id);

    const
        {
            data: cocktailRatings,
            isLoading: ratingsLoading
        } = useGetCocktailRatingsQuery(cocktail_id);

    const
        {
            data: averageRating,
            isLoading: averageLoading
        } = useGetAvgRatingsQuery(cocktail_id);

    const
        {
            data: countRatings,
            isLoading: countLoading
        } = useGetRatingsCountQuery(cocktail_id);

    const
        {
            data: userRating,
            isLoading: userRatingLoading
        } = useGetOneUserRatingQuery(cocktail_id);

    const
        [
            updateOrCreateRating
        ] = useUpdateOrCreateRatingMutation();

    const
        [
            ratings,
            setCocktailRatings
        ] = useState([]);

    const
        [
            isCreator,
            setCreator
        ] = useState(false);

    const
        [
            isRating,
            setIsRating
        ] = useState(false);

    const
        [
            newRating,
            setRating
        ] = useState(0);

    const
        [
            avgRatings,
            setAvg
        ] = useState(0);

    const
        [
            newReview,
            setReview
        ] = useState("");

    const
        [
            alcohol,
            setAlcoholic
        ] = useState("");

    const
        [
            stars,
            setStars
        ] = useState({
            "1": false,
            "2": false,
            "3": false,
            "4": false,
            "5": false
        })

    const handleRateThis = () => {
        setIsRating(true);

        if (userRating !== null) {
            let currentRating = userRating.rating
            let currentStars = RatingSetter(stars, currentRating)
            setRating(currentRating);
            setReview(userRating.review);
            setStars(currentStars)

        }
    }

    const handleRating = async (event) => {
        let starRate = event.target.ownerDocument
            .activeElement.value
        setRating(starRate);
        let newStars = RatingSetter(stars, starRate)
        setStars(newStars)

    }

    const handleUpdate = () => {
        navigate(
            `/cocktails/update/${cocktail_id}`
        );
    }

    const handleReview = (event) => {
        setReview(event.target.value);
    }

    const submitRating = async (event) => {
        event.preventDefault();
        const data = {
            "review": newReview,
            "rating": newRating
        };
        updateOrCreateRating(
            {
                cocktail_id: cocktail_id,
                data: data
            }
        );
        setRating(0);
        setReview("");
        setIsRating(false);
    }

    useEffect(() => {
        if (
            !accountLoading &&
            !account
        ) {
            navigate("/login")
        }

        else if
            (
            account &&
            cocktail &&
            cocktailRatings &&
            averageRating &&
            countRatings
        ) {
            setCocktailRatings(
                cocktailRatings["ratings"]
            );
            setAvg(
                averageRating["ratings_average"]
                    .toPrecision(2)
            );
            setAlcoholic(
                cocktail.alcoholic
            );

            if (account.account.id == cocktail.account_id) {
                setCreator(true)
            }

            if (cocktail.alcoholic) {
                setAlcoholic("Alcoholic");
            }
            else {
                setAlcoholic("Non Alcoholic")
            }
        }
    }, [
        cocktailRatings,
        account,
        cocktail,
        cocktailRatings,
        averageRating,
        countRatings
    ]
    )

    if (
        accountLoading ||
        cocktailLoading ||
        ratingsLoading ||
        averageLoading ||
        countLoading ||
        userRatingLoading
    ) {
        return (<div>Loading</div>)
    }
    else {

        return (
            <div className="container detail-container p-0">
                <div className="card detail-container">
                    <div className="card-header detail-round-top" >
                        <div className="text-center"><h1>{cocktail.cocktail_name}</h1></div>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col">
                                <div className="d-flex justify-content-center">
                                    <img src={cocktail.image_url} className="card-img-top rounded-circle detail-cocktail-img"></img>
                                </div>
                            </div>
                            <div className="col align-self-center">
                                <div className="row">
                                    <h5 className="ps-5 text-center">
                                        <strong>Users Verdict: </strong>
                                        {avgRatings}
                                        <StarFilledIcon />
                                    </h5>
                                    <h6 className="ps-5 text-center">Of {countRatings.ratings_count} Ratings</h6>
                                </div>
                                <div className="row  favorite-button">
                                    <div className="col-md-4" ></div>
                                    <div className="col"><FavoriteToggleButton cocktail={cocktail} /></div>
                                </div>
                                {isCreator &&
                                    <div className="row">
                                        <div className="d-flex justify-content-center">
                                        <div className="row p-2">
                                            <div className="d-flex justify-content-center">
                                                <button onClick={handleUpdate}>Update</button>
                                            </div>
                                        </div>
                                        <div className="row p-2">
                                            <div className="d-flex justify-content-center">
                                                <CocktailDeleteButton props={cocktail.id} />
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                        <div className="container detail-round-top">
                            <div className="row align-items-center">
                                <div className="col">
                                    <h4 className="ps-4 text-center"><strong>{alcohol}</strong></h4>
                                </div>
                                <div className="col-sm-1"></div>
                            </div>
                            <div className="row align-items-end">
                                <div className="row align-items-center">
                                    <div className="col-sm-6">
                                        <h2 className="text-center">What you need</h2>
                                    </div>
                                    <div className="col-sm-6 text-center">
                                        <h2>Instructions</h2>
                                    </div>
                                </div>
                                <div className="row align-items-center">
                                    <div className="col-sm-6 text-start">
                                        <ul>
                                            <li><strong>Suggested Serving Glass:</strong> {cocktail.suggested_serving_glass}</li>
                                            <li><strong>Ingredients:</strong> {cocktail.ingredients}</li>
                                        </ul>
                                    </div>
                                    <div className="col-sm-6 text-center">
                                        <p>{cocktail.instructions}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="container detail-round-bot">
                            <div className="container p-3">
                                <div className="row">
                                    {!isRating &&
                                        <>
                                            <div className="col">
                                                <h2 className="align-self-start">Reviews:</h2>
                                            </div>
                                            <div className="col">
                                                <div className="d-flex justify-content-end">
                                                    <button className="align-self-end" onClick={handleRateThis}>Rate this cocktail</button>
                                                </div>
                                            </div>
                                        </>}
                                    {isRating &&
                                        <div className="d-flex justify-content-around">
                                            <div className="flex-col"><h3>Rate and Review: </h3></div>
                                            <div className="flex-col" onClick={handleRating} value={newRating}>
                                                {Object.entries(stars)
                                                    .map(([star, checked]) => {
                                                        return (
                                                            <button key={star} className="detail-star" value={star}>
                                                                {!checked ? <StarIcon /> : <StarFilledIcon />}
                                                            </button>)
                                                    })}

                                            </div>
                                            <div className="flex-col">
                                                <button form="rating-form" className="btn detail-rate-button" type="submit" value={newReview}>Rate</button>
                                            </div>
                                        </div>
                                    }
                                </div>
                                <div className="row">
                                    <div className="row overflow-auto detail-rating-box">
                                        {!isRating && ratings.map(rating => {
                                            return (
                                                <div className="container" key={rating.id}>
                                                    <div className="row">
                                                        <div className="col-sm-8">
                                                            <h6><strong>{rating["account_name"]} said:</strong></h6>
                                                        </div>
                                                        <div className="col-sm-4">
                                                            <h6 className="ps-5 text-end">
                                                                <strong>Rated</strong>
                                                                {rating.rating}
                                                                <StarFilledIcon />
                                                            </h6>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-sm-8">
                                                            <p className="ps-5">{rating.review}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })}
                                        {isRating &&
                                            <form id="rating-form" onSubmit={submitRating}>
                                                <textarea onChange={handleReview} rows="5" className="container-fluid detail-text-box" type="text" value={newReview}></textarea>
                                            </form>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CocktailDetails;
